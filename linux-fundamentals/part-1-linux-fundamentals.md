# Part 1 - Learn Linux Fundamentals

## Objective

Learn to run some of the first essential commands on an interactive terminal.

## Notes

### Background

* The first release of a Linux operation system was on September 17, 1991 by Linus Torvalds.
* Linux is based on UNIX (another operating system).
* Most common disturbutions on Linux are Ubuntu and Debian.
* Each Linux disturbution can consit of versions like Windows (Windows 7, 10, etc).
* Ubuntu can ran on system with only 512 MB of RAM.

### Very first commands

* `echo`: outputs any text that we provide
* `whoami`: find out what user you are currently logged in as
* `ls`: listing files/folders in our working directory
* `ls <folder_name>`: list files/folder of the a directory
* `cat /home/ubuntu/Documents/todo.txt`: outputs content of a file within a directory
* `cd`: navigate/change directory
* `pwd`: print the full path of our current working directory
* `cat`: outputs the content of a file

### Searching for files

* `find -name <filename>`: search for a file and outputs its path.
* `find -name *.txt`: search for a file based on its extension.
* `grep "<entry>" <filepath>`: search the entire file to find out the desired entry.
* `grep -i "<entry>" <filepath>`: performs case-insensitive search.